import React from 'react';
import { Row, Col } from 'react-bootstrap';

function WeatherRow(props) {
  const { daysNames, currentCityName, currentCityData } = props;

  if (!currentCityName || !daysNames || !currentCityData) {
    return <Row></Row>;
  }

  return (
    <Row>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="p10 m10">
          <h3>{currentCityName}</h3>  
        </div>
      </Col>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="infoBox p10 m10">
          <h5>Today</h5>
          <p>Min: {currentCityData[0][0]}°C</p>
          <p>Max: {currentCityData[0][1]}°C</p>
        </div>
      </Col>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="infoBox p10 m10">
          <h5>Tomorrow</h5>
          <p>Min: {currentCityData[1][0]}°C</p>
          <p>Max: {currentCityData[1][1]}°C</p>
        </div>
      </Col>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="infoBox p10 m10">
          <h5>{daysNames[2]}</h5>
          <p>Min: {currentCityData[2][0]}°C</p>
          <p>Max: {currentCityData[2][1]}°C</p>
        </div>
      </Col>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="infoBox p10 m10">
          <h5>{daysNames[3]}</h5>
          <p>Min: {currentCityData[3][0]}°C</p>
          <p>Max: {currentCityData[3][1]}°C</p>
        </div> 
      </Col>
      <Col xs={12} sm={12} md={4} lg={2}>
        <div className="infoBox p10 m10">
          <h5>{daysNames[4]}</h5>
          <p>Min: {currentCityData[4][0]}°C</p>
          <p>Max: {currentCityData[4][1]}°C</p>
        </div>
      </Col>
    </Row>
  )
}

export default WeatherRow;