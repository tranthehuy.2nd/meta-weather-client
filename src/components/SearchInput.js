import React from 'react';
import { Form } from 'react-bootstrap';

function SearchInput(props) {

  const onSearch = (e) => {
    const newKeyword = e.target.value;
    if (props.onSearch) {
      props.onSearch(newKeyword);
    }
  }

  const _onBlur = (e) => {
    onSearch(e);
  }

  const _onKeyDown = (e) => {
    if (e.key === 'Enter') {
      onSearch(e);
    }
  }

  return (
    <Form.Group controlId="formSearch">
      <Form.Control data-testid="inputSearch" placeholder="Search..." onBlur={_onBlur} onKeyDown={_onKeyDown} />
      <Form.Text className="text-muted">
        A name of city. For example: Ho Chi Minh, Sydney, New York,...
      </Form.Text>
    </Form.Group>
  );
}

export default SearchInput;