import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';

function CitiesList(props) {
  const { cities, onClick } = props;
  if (cities.length > 0) {
    return (
      <Row>
        <Col>
          <p>Which city do you wanna search?</p>
          <ul>
            {cities.map((city) => (
              <li 
                key={city.woeid}
                onClick={() => { if (onClick) onClick(city); }}
              >
                <Button variant="link">{city.title}</Button>
              </li>
            ))}
          </ul>
        </Col>
      </Row>
    )
  }
  return <Row></Row>;
}

export default CitiesList;