import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './App.css';
import moment from 'moment';

import SearchInput from './components/SearchInput';
import WeatherRow from './components/WeatherRow';
import CitiesList from './components/CitiesList';

import searchLocationsService from './services/searchLocations';
import getLocationInfoService from './services/getLocationInfo';

function App(props) {
  // calculate next 5 days
  const today = moment();
  const next1day = moment().add(1, 'days');
  const next2days = moment().add(2, 'days');
  const next3days = moment().add(3, 'days');
  const next4days = moment().add(4, 'days');
  const days = [today, next1day, next2days, next3days, next4days];
  const daysNames = days.map(d => d.format('ddd MMM Do'));
  const daysValues = days.map(d => d.format('YYYY/M/D'));
  
  // init default states for app
  const [keyword, setKeyword] = useState(props.keyword || "");
  const [cities, setCities] = useState(props.cities || []);
  const [currentCityName, setCurrentCityName] = useState(props.currentCityName || '');
  const [currentCityData, setCurrentCityData] = useState(props.currentCityData || []);

  // init default fetchService for app
  const searchLocations = props.searchLocationsService || searchLocationsService;
  const getLocationInfo = props.getLocationInfoService || getLocationInfoService;

  useEffect(() => {
    if (keyword) {
      searchLocations(keyword)
        .then((result) => {
          setCities(result);
        })
        .catch(err => {
          console.error(err);
        });
    }
  }, [searchLocations, keyword]);

  const onSearch = (newKeyword) => {
    setKeyword(newKeyword);
  }

  const getMaxMinADay = (dayRecords) => {
    let max = -100;
    let min = 1000;
    for (let index = 0; index < dayRecords.length; index++) {
      const element = dayRecords[index];
      if (element.max_temp > max) max = element.max_temp;
      if (element.min_temp < min) min = element.min_temp;
    }
    return [Math.round(min), Math.round(max)];
  }

  const getCityInfo = (city) => {
    const cityId = city.woeid;
    Promise.all(
      daysValues.map(d => getLocationInfo(cityId, d))
    ).then(results => {
      const maxMinArray = results.map(r => getMaxMinADay(r));
      setCurrentCityData(maxMinArray);
      setCurrentCityName(city.title);
    })
  }

  return (    
    <div className="app">
      <div className="header-wrapper pt10">
        <Container className="header">
          <Row>
            <Col>
              <h3>MetaWeather Client App</h3>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="content mt10 mb10">
        <Row>
          <Col>
            <SearchInput onSearch={onSearch}/>
          </Col>
        </Row>
        <WeatherRow 
          currentCityData={currentCityData} 
          currentCityName={currentCityName} 
          daysNames={daysNames}
        />
        <CitiesList cities={cities} onClick={getCityInfo}/>
      </Container>
    </div>
  );
}

export default App;
