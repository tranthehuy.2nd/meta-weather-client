function getLocationInfo(cityId, dateStr) {
  if (cityId && dateStr) {
    return fetch(`/api/location/${cityId}/${dateStr}/`).then(res => res.json())
  }
  return Promise.resolve();
}

export default getLocationInfo;