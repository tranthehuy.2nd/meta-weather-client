function searchLocations(str) {
  const keyword = `${str}`.trim();
  if (keyword) {
    return fetch(`/api/location/search/?query=${keyword}`).then(res => res.json());
  }
  return Promise.resolve();
}

export default searchLocations;