import React from 'react';
import { act } from 'react-dom/test-utils';
import { render } from '@testing-library/react';
import App from './App';

test('renders app title', () => {
  const { getByText } = render(<App />);
  const titleElement = getByText(/MetaWeather Client App/i);
  expect(titleElement).toBeInTheDocument();
});

test('renders cities list when having keyword', async () => {
  let container;
  await act(async () => {
    const mockData = `[{"title":"Ho Chi Minh City","location_type":"City","woeid":1252431,"latt_long":"10.759180,106.662498"}]`;
    const mockReturn = Promise.resolve(JSON.parse(mockData));
    const searchLocationsService = () => mockReturn;
    container = render(<App 
      keyword="Ho chi minh"
      searchLocationsService={searchLocationsService}
    />);
  });

  const { getByText } = container;
  const cityNameElement = getByText(/Ho Chi Minh City/i);
  expect(cityNameElement).toBeInTheDocument();
});


test('renders temperature of a city when loading a city info', async () => {
  let container;
  const mockData = `[[11,22],[33,44],[55,66],[77,88],[99,100]]`;
  container = render(<App 
    currentCityName="Ho Chi Minh City"
    currentCityData={JSON.parse(mockData)}
  />);

  const { getByText } = container;
  const firstMinTemp = getByText(/11/i);
  expect(firstMinTemp).toBeInTheDocument();

  const lastMaxTemp = getByText(/100/i);
  expect(lastMaxTemp).toBeInTheDocument();
});